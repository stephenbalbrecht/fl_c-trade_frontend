import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplexStrategy, StrategyService } from '../common/strategy-service/strategy.service';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { Stock } from '../common/stock-service/stock.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-strategy-details',
  templateUrl: './strategy-details.component.html',
  styleUrls: ['./strategy-details.component.css']
})
export class StrategyDetailsComponent implements OnInit, OnDestroy {

  id: number;
  public strategy: ComplexStrategy;
  private sub: any;

  faArrowCircleLeft = faArrowCircleLeft;

  constructor(private route: ActivatedRoute, private router: Router, private strategyService: StrategyService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const key = 'id';
      this.id = +params[key];

      this.getStrategy();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  stopStrategy() {
    // TODO: Send action to stop strategy
    // Set strategy stopped
    this.strategy.stopped = new Date().toISOString();

    this.strategyService.insertStrategy(this.strategy).subscribe(
      response => {
        // TODO Provide feedback
      }, error => {
        // TODO Provide feedback
      }
    );
  }

  getStrategy(): void {
    this.strategyService.getStrategyById(this.id).subscribe(
      response => {
        this.strategy = response;
      },
      error => {
        // this.router.navigateByUrl('/404');
      }
    );
  }
}
