import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StrategiesComponent } from './strategies/strategies.component';
import { StrategyDetailsComponent } from './strategy-details/strategy-details.component';

const routes: Routes = [
  { path: '', component: StrategiesComponent },
  { path: 'strategy', component: StrategiesComponent },
  { path: 'strategy/:id', component: StrategyDetailsComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
