import { Component, OnInit } from '@angular/core';
import { ComplexStrategy, StrategyService } from '../common/strategy-service/strategy.service';

@Component({
  selector: 'app-strategies',
  templateUrl: './strategies.component.html',
  styleUrls: ['./strategies.component.css']
})
export class StrategiesComponent implements OnInit {

  public activeStrategies: ComplexStrategy[];
  public closedStrategies: ComplexStrategy[];

  constructor(private strategyService: StrategyService) { }

  ngOnInit() {
    this.getActiveStrategies();
    this.getClosedStrategies();
  }

  getActiveStrategies(): void {
    this.strategyService.getActiveStrategies().subscribe(response => {
      this.activeStrategies = response;
    }, error => {
      this.activeStrategies = [];
      // Encourage strategy creation;
    });
  }

  getClosedStrategies(): void {
    this.strategyService.getInactiveStrategies().subscribe(response => {
      this.closedStrategies = response;
    }, error => {
      this.closedStrategies = [];
    });
  }
}
