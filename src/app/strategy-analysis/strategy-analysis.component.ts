import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { ComplexStrategy } from '../common/strategy-service/strategy.service';
import { faLongArrowAltDown, faLongArrowAltUp } from '@fortawesome/free-solid-svg-icons';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { PriceService, Price } from '../common/price-service/price.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-strategy-analysis',
  templateUrl: './strategy-analysis.component.html',
  styleUrls: ['./strategy-analysis.component.css']
})
export class StrategyAnalysisComponent implements OnInit, OnDestroy {

  @Input() strategy: ComplexStrategy;
  @ViewChild(BaseChartDirective, {read: BaseChartDirective, static: true}) chart: BaseChartDirective;
  private prices: Price[];
  private timer: any;

  public chartData: ChartDataSets[] = [{data: [], label: 'Stock Price'}];
  public chartType = 'line';
  public chartLabels: Label[] = [];
  public chartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        // type: 'time',
        ticks: {
            autoSkip: true,
            maxTicksLimit: 12
        }
      }]
    },
    elements: { point: { radius: 1, hitRadius: 1, hoverRadius: 4 } }
  };
  public chartColors: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ];

  faLongArrowAltDown = faLongArrowAltDown;
  faLongArrowAltUp = faLongArrowAltUp;

  constructor(private priceService: PriceService) {}

  ngOnInit() {
    this.getPrices();
  }

  ngOnDestroy() {
    this.timer.unsubscribe();
  }

  getPrices() {
    this.priceService.getAll(this.strategy.stock.ticker).subscribe(response => {
      this.prices = response;
      this.setChartData();
    }, error => { });

    this.setPriceTimer();
  }

  private setPriceTimer() {
    this.timer = timer(5000).subscribe(() => {
      this.getPrices();
    });
  }
  private setChartData(): void {
    this.chartData[0].data.length = 0;
    this.chartLabels.length = 0;
    for (const p of this.prices) {
      this.chartData[0].data.push(p.price);
      this.chartLabels.push(p.recordedAt);
    }
    this.chart.update();
  }
}
