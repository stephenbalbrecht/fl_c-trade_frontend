import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stock } from '../stock-service/stock.service';

export class Price {
  constructor(
    public id: number,
    public stock: Stock,
    public price: number,
    public recordedAt: string
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  constructor(private httpClient: HttpClient) { }

  getAll(ticker: string): Observable<Price[]> {
    return this.httpClient.get<Price[]>(`api/price/${ticker}`);
  }
}
