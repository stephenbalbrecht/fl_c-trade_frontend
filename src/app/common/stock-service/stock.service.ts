import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class Stock {
  constructor(
    public id: number,
    public ticker: string
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>('api/stock');
  }

  getStockById(id: number): Observable<Stock> {
    return this.httpClient.get<Stock>(`api/stock/${id}`);
  }

  getStockByTicker(ticker: string): Observable<Stock> {
    return this.httpClient.get<Stock>(`api/stock/${ticker}`);
  }

  create(stock: Stock): Observable<Stock> {
    return this.httpClient.post<Stock>(`api/stock`, stock);
  }
}
