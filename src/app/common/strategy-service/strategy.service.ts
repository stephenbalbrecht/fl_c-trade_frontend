import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Stock } from '../stock-service/stock.service';

export class ComplexStrategy {
  constructor(
    public id: number,
    public stock: Stock,
    public size: number,
    public exitProfitLoss: number,
    public currentPosition: number,
    public lastTradePrice: number,
    public profit: number,
    public stopped: string
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class StrategyService {

  constructor(private httpClient: HttpClient) { }

  getStrategies(): Observable<ComplexStrategy[]> {
    return this.httpClient.get<ComplexStrategy[]>('api/strategy').pipe(
      catchError(this.handleError)
    );
  }

  getActiveStrategies(): Observable<ComplexStrategy[]> {
    return this.httpClient.get<ComplexStrategy[]>('api/strategy/active').pipe(
      catchError(this.handleError)
    );
  }

  getInactiveStrategies(): Observable<ComplexStrategy[]> {
    return this.httpClient.get<ComplexStrategy[]>('api/strategy/inactive').pipe(
      catchError(this.handleError)
    );
  }

  getStrategyById(id: number): Observable<ComplexStrategy> {
    return this.httpClient.get<ComplexStrategy>(`api/strategy/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  insertStrategy(strategy: ComplexStrategy): Observable<ComplexStrategy> {
    return this.httpClient.post<ComplexStrategy>(`/api/strategy`, strategy).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred: ', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}`);
    }

    return throwError('Something bad happened; please try again later.');
  }
}
