import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stock } from '../stock-service/stock.service';
import { ComplexStrategy } from '../strategy-service/strategy.service';

export class Trade {
  constructor(
      public id: number,
      public stock: Stock,
      public stockTicker: string,
      public price: number,
      public size: number,
      public tradeType: string,
      public state: string,
      public lastStateChange: string,
      public created: string,
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>('api/trade');
  }

  getTradeByStrategy(strategyId: number): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>(`api/trade/strategy/${strategyId}`);
  }

}
