import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { NewStrategyDialogComponent } from './new-strategy-dialog/new-strategy-dialog.component';
import { StrategiesComponent } from './strategies/strategies.component';
import { StrategyDetailsComponent } from './strategy-details/strategy-details.component';
import { TradeHistoryComponent } from './trade-history/trade-history.component';
import { StrategyAnalysisComponent } from './strategy-analysis/strategy-analysis.component';
import { StrategyTableComponent } from './strategy-table/strategy-table.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    NewStrategyDialogComponent,
    StrategiesComponent,
    StrategyDetailsComponent,
    TradeHistoryComponent,
    StrategyAnalysisComponent,
    StrategyTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
