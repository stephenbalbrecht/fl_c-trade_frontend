import { Component, OnInit, Input } from '@angular/core';
import { ComplexStrategy } from '../common/strategy-service/strategy.service';

@Component({
  selector: 'app-strategy-table',
  templateUrl: './strategy-table.component.html',
  styleUrls: ['./strategy-table.component.css']
})
export class StrategyTableComponent implements OnInit {

  @Input() strategies: ComplexStrategy[];

  constructor() { }

  ngOnInit() {
  }

}
