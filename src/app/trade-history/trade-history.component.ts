import { Component, OnInit, Input } from '@angular/core';
import { Trade, TradeService } from '../common/trade-service/trade.service';
import { ComplexStrategy } from '../common/strategy-service/strategy.service';

@Component({
  selector: 'app-trade-history',
  templateUrl: './trade-history.component.html',
  styleUrls: ['./trade-history.component.css']
})
export class TradeHistoryComponent implements OnInit {

  @Input() strategy: ComplexStrategy;
  public trades: Trade[];

  constructor(private tradeService: TradeService) { }

  ngOnInit() {
    this.tradeService.getTradeByStrategy(this.strategy.id).subscribe(response => {
      // TODO Only display FILLED trades
      this.trades = [];
      for (const t of response) {
        if (t.state === 'FILLED') {
          t.lastStateChange = t.lastStateChange.slice(t.lastStateChange.indexOf('T') + 1);
          this.trades.push(t);
        }
      }
      // this.trades = response;
    }, error => {
      this.trades = [];
    });
  }

}
