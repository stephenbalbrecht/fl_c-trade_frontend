import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewStrategyDialogComponent } from './new-strategy-dialog.component';

describe('NewStrategyDialogComponent', () => {
  let component: NewStrategyDialogComponent;
  let fixture: ComponentFixture<NewStrategyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewStrategyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewStrategyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
