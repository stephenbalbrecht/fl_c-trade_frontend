import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ComplexStrategy, StrategyService } from '../common/strategy-service/strategy.service';
import { StockService, Stock } from '../common/stock-service/stock.service';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-new-strategy-dialog',
  templateUrl: './new-strategy-dialog.component.html',
  styleUrls: ['./new-strategy-dialog.component.css']
})
export class NewStrategyDialogComponent implements OnInit, OnDestroy {

  @Input() strategy: ComplexStrategy;
  @Output() updateStrategiesEmitter = new EventEmitter();

  private eventsSubscription: any;

  @ViewChild('strategyModal', {read: ElementRef, static: true})
  strategyModal: ElementRef;
  @ViewChild('title', {read: ElementRef, static: true})
  title: ElementRef;
  @ViewChild('confirmBtn', {read: ElementRef, static: true})
  confirmBtn: ElementRef;
  @ViewChild('stockInput', {read: ElementRef, static: true})
  stockInput: ElementRef;
  @ViewChild('sizeInput', {read: ElementRef, static: true})
  sizeInput: ElementRef;
  @ViewChild('exitProfitLossInput', {read: ElementRef, static: true})
  exitProfitLossInput: ElementRef;

  private isNewStrategy = true;
  private stocks: Stock[];
  stockList: any;
  lastKeyDown = 0;

  constructor(private stockService: StockService,
              private strategyService: StrategyService) { }

  ngOnInit() {

    this.restrictStockInput();
    this.restrictSizeInput();
    this.restrictExitInput();

    this.stockService.getAll().subscribe(response => {
      this.stocks = response;
    }, error => {
      this.stocks = [];
    });
  }

  ngOnDestroy() {
  }

  public submitStrategy(f: NgForm): void {
    // const stock = this.stockInput.nativeElement.value.replace(/ /g, '');
    const stockInput = $('#stockInput')[0];
    const sizeInput = $('#sizeInput')[0];
    const exitProfitLossInput = $('#exitProfitLossInput')[0];

    const stock = stockInput.value.replace(/ /g, '');
    const size = sizeInput.value;
    const exit = exitProfitLossInput.value;

    let valid = true;

    if (!stock.match('^[A-Za-z0-9]{1,5}$')) {
      stockInput.classList.remove('is-valid');
      stockInput.classList.add('is-invalid');
      valid = false;
    } else {
      stockInput.classList.remove('is-invalid');
      stockInput.classList.add('is-valid');
    }

    if (size < 0 || size === '') {
      sizeInput.classList.remove('is-valid');
      sizeInput.classList.add('is-invalid');
      valid = false;
    } else {
      sizeInput.classList.remove('is-invalid');
      sizeInput.classList.add('is-valid');
    }

    if (exit < 0 || exit === '') {
      exitProfitLossInput.classList.remove('is-valid');
      exitProfitLossInput.classList.add('is-invalid');
      valid = false;
    } else {
      exitProfitLossInput.classList.remove('is-invalid');
      exitProfitLossInput.classList.add('is-valid');
    }

    if (!valid) {
      return;
    }
    this.isNewStrategy = (this.strategy === undefined);
    let strategy;

    if (this.isNewStrategy) {
      strategy = new ComplexStrategy(-1, new Stock(-1, stock), size,
          exit, null, null, null, null);
    } else {
      strategy = this.strategy;
      strategy.exitProfitLoss = exit;
    }

    this.confirmBtn.nativeElement.setAttribute('disabled', 'true');
    this.confirmBtn.nativeElement.innerHTML = `<span class="spinner-border `
        + `spinner-border-sm mr-2" role="status" aria-hidden="true"></span>`
        + `Loading...`;


    this.strategyService.insertStrategy(strategy).subscribe(response => {
      this.confirmBtn.nativeElement.removeAttribute('disabled');
      this.confirmBtn.nativeElement.innerHTML = (this.isNewStrategy)
      ? 'Create New Strategy'
      : 'Edit Strategy';

      this.clearModal();
      this.updateStrategies();
      // TODO Give feedback on response
    }, error => {
      this.confirmBtn.nativeElement.removeAttribute('disabled');
      this.confirmBtn.nativeElement.innerHTML = (this.isNewStrategy)
      ? 'Create New Strategy'
      : 'Edit Strategy';

      this.updateStrategies();
      this.clearModal();
      // TODO Give feedback on error
    });
  }

  fillFieldsIfStrategyExists(): void {
    if (this.strategy !== undefined) {
      this.isNewStrategy = false;
    }
  }

  getStocks($event) {

    const stock = this.stockInput.nativeElement.value;

    this.stockList = [];

    if (stock.length > 0) {
      if ($event.timeStamp - this.lastKeyDown > 200) {
        this.stockList = this.searchStocks(this.stocks, stock);

        $(this.stockInput.nativeElement).autocomplete({
          source: this.stockList,
          messages: {
            noResults: '',
            results: () => {}
          }
        });
      }
    }

  }

  searchStocks(arr, regex) {
    const matches = [];
    for (const a of arr) {
      if (a.ticker.match(regex)) {
        matches.push(a.ticker);
      }
    }
    return matches;
  }

  private restrictStockInput(): void {
    $('input#stockInput').on({
      keydown: (e) => {
        if (e.key === ' ') {
          return false;
        }
      },
      change() {
        this.value = this.value.replace(/ /g, '');
      }
    });
  }

  private restrictSizeInput(): void {
    $('input#sizeInput').on({
      keydown(e) {
        if (e.key === '.' || e.key === '-' || e.key === 'e') {
          return false;
        }
      },
      change() {
        this.value = this.value.replace(/[^0-9]+/g, '');
      }
    });
  }

  private restrictExitInput(): void {
    $('input#exitProfitLossInput').on({
      keydown(e) {
        if (e.key === '-' || e.key === 'e') {
          return false;
        }
      },
      change() {
        this.value = this.value.replace(/[^0-9\.]+/g, '');
      }
    });
  }

  private clearModal(): void {
    const stockInput = $('#stockInput')[0];
    const sizeInput = $('#sizeInput')[0];
    const exitProfitLossInput = $('#exitProfitLossInput')[0];

    stockInput.classList.remove('is-valid');
    sizeInput.classList.remove('is-valid');
    exitProfitLossInput.classList.remove('is-valid');

    if (this.isNewStrategy) {
      stockInput.value = '';
      sizeInput.value = '';
      exitProfitLossInput.value = '1.0';
    }

    this.strategyModal.nativeElement.click();
  }

  updateStrategies() {
    this.updateStrategiesEmitter.emit();
  }
}
