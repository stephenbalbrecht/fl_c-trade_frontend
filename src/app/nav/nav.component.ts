import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onButtonGroupClick($event) {
    const clickedElement = $event.target || $event.srcElement;

    if (clickedElement.nodeName === 'A') {

      const isCertainButtonActive = clickedElement.parentElement.parentElement.querySelector('.active');

      if (isCertainButtonActive) {
        isCertainButtonActive.classList.remove('active');
      }

      clickedElement.className += ' active';

    }
  }

}
